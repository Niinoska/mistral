from django import forms
from .models import Formulario_Persona
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from .models import Perfil


class Formulario_Compra(forms.ModelForm):

    class Meta:
        model = Formulario_Persona
        fields = ('nombre', 'rut', 'correo', 'fecha', 'password', 'region','cuidad', )
        labels = {
            'nombre': 'Nombre y apellido',
            'rut': 'Rut',
            'correo': 'Correo',
            'fecha': 'Fecha de nacimiento',
            'password': 'Contraseña',
            'region': 'Region',
            'ciudad': 'Ciudad',
            
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'rut': forms.TextInput(attrs={'class': 'form-control'}),
            'correo': forms.EmailInput(attrs={'class': 'form-control'}),
            'fecha': forms.DateInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'}),
            'region': forms.Select(attrs={'class': 'form-control'}),
            'ciudad': forms.Select(attrs={'class': 'form-control'}),
            
        }

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=140, required=True)
    last_name = forms.CharField(max_length=140, required=False)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )
