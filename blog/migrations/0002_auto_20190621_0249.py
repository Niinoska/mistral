# Generated by Django 2.1.2 on 2019-06-21 06:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Formulario_Copete',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imagen', models.ImageField(upload_to='')),
                ('nombre', models.CharField(max_length=15)),
                ('tipo', models.CharField(max_length=15)),
                ('descripcion', models.TextField(max_length=15)),
                ('estado', models.CharField(choices=[('disponible', 'Disponible'), ('nodisponible', 'No Disponible')], max_length=25)),
                ('fecha', models.DateField()),
            ],
        ),
        migrations.DeleteModel(
            name='Formulario_Perro',
        ),
        migrations.RemoveField(
            model_name='Formulario_Persona',
            name='vivienda',
        ),
    ]
