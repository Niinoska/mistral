# Generated by Django 2.1.2 on 2019-06-28 21:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20190628_1732'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulario_copete',
            name='descripcion',
            field=models.TextField(max_length=300),
        ),
        migrations.AlterField(
            model_name='formulario_copete',
            name='estado',
            field=models.CharField(choices=[('disponible', 'Disponible'), ('seleccionado', 'Seleccionado'), ('nodisponible', 'No Disponible')], max_length=25),
        ),
    ]
