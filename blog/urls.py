from django.conf.urls import include, url
from . import views
from django.urls import path, include

app_name="blog"
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^comprar/', views.comprar, name='comprar'),
    url(r'^contacto/$', views.contacto, name='contacto'),
    url(r'^finalizar/$', views.finalizar, name='finalizar'),
    url(r'^finalizar2/$', views.finalizar2, name='finalizar2'),
    url(r'^copete/(?P<pk>[0-9]+)/$', views.copete_detail, name='copete_detail'),
    url('^', include('django.contrib.auth.urls')),


]

