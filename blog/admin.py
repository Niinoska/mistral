from django.contrib import admin
from .models import Formulario_Copete
from .models import Formulario_Persona
from .models import Perfil

admin.site.register(Formulario_Copete)
admin.site.register(Formulario_Persona)

@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'bio', 'web')