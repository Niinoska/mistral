from django.shortcuts import render, redirect
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from .models import Formulario_Copete, Perfil
from .forms import Formulario_Compra, SignUpForm
from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.views.generic import CreateView, TemplateView



def index(request):
    return render(request, 'blog/index.html', {})

def contacto(request):
    return render(request, 'blog/contacto.html', {})

def finalizar(request):
    return render(request, 'blog/finalizar.html', {})

def finalizar2(request):
    return render(request, 'blog/finalizar2.html', {})


def comprar(request):
    copetes = Formulario_Copete.objects.all()
    return render(request, 'blog/comprar.html', {'copetes': copetes})


def copete_detail(request, pk):
    copete = get_object_or_404(Formulario_Copete, pk=pk)
    if request.method == "POST":
        form = Formulario_Compra(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.usuario = request.user
            post.save()
            copete = Formulario_Copete.objects.get(pk=pk)
            copete.estado = 'seleccionado'
            copete.save()
            return redirect('blog:comprar')
    else:
        form = Formulario_Compra()
    return render(request, 'blog/copete_detail.html', {'copete': copete, 'form': form})

class SignUpView(CreateView):
    model = Perfil
    form_class = SignUpForm

    def form_valid(self, form):
        '''
        En este parte, si el formulario es valido guardamos lo que se obtiene de él 
        y usamos authenticate para que el usuario incie sesión luego de haberse registrado 
        y lo redirigimos al index
        '''
        form.save()
        usuario = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        usuario = authenticate(username=usuario, password=password)
        login(self.request, usuario)
        return redirect('/')


class BienvenidaView(TemplateView):
    template_name = 'blog/bienvenida.html'


from django.contrib.auth.views import LoginView


class SignInView(LoginView):
    template_name = 'blog/iniciar_sesion.html'


from django.contrib.auth.views import LoginView, LogoutView


class SignOutView(LogoutView):
    template_name = 'blog/index.html'
    pass

