from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.utils import timezone
from django.db import models
from PIL import Image, ImageOps
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save



class Formulario_Persona (models.Model):
    correo = models.EmailField(max_length=25)
    rut = models.CharField(max_length=12)
    nombre = models.CharField(max_length=25)
    def __str__(self):
        return self.nombre
    fecha = models.DateField()
    password = models.CharField(max_length=50)
    ESTADO_REGION = (
        ('estado_1', 'I Tarapaca'),
        ('estado_2', 'II Antofagasta'),
        ('estado_3', 'III Atacama'),
        ('estado_4', 'IV Coquimbo'),
        ('estado_5', 'V Valparaiso'),
        ('estado_6', 'VI Ohiggins'),
        ('estado_7', 'VII Maule'),
        ('estado_8', 'VIII Bio Bio'),
        ('estado_9', 'IX Araucania'),
        ('estado_10', 'X Los Lagos'),
        ('estado_11', 'XI Aisen'),
        ('estado_12', 'XII Magallanes y Antartica'),
        ('estado_13', 'XII Metropolitana'),
        ('estado_14', 'IVX Los Rios'),
        ('estado_15', 'XV Arica y Parinacota'),
    )

    region = models.CharField(
        max_length=25, choices=ESTADO_REGION, default='region')

    ESTADO_CUIDAD = (
        ('1', 'Arica'),
        ('2', 'Alto Hospicio'),
        ('3', 'Iquique'),
        ('4', 'Pozo Almonte'),
        ('5', 'Caldera'),
        ('6', 'Chanaral'),
        ('7', 'Copiapo'),
        ('8', 'Diego de Almagro'),
        ('9', 'El Salvador    Huasco'),
        ('10', 'Tierra Amarilla'),
        ('11', 'Vallenar'),
        ('12', 'Andacollo'),
        ('13', 'Combarbala'),
        ('14', 'Coquimbo'),
        ('15', 'El Palqui'),
        ('16', 'Illapel'),
        ('17', 'La Serena'),
        ('18', 'Los Vilos'),
        ('19', 'Montepatria'),
        ('20', 'Ovalle'),
        ('21', 'Salamanca'),
        ('22', 'Vicuna'),
        ('23', 'Algarrobo'),
        ('24', 'Cabildo'),
        ('25', 'Calle Larga'),
        ('26', 'Cartagena'),
        ('27', 'Quilpue'),
        ('28', 'Valparaiso'),
        ('29', 'Villa Alemana'),
    )

    cuidad = models.CharField(
        max_length=25, choices=ESTADO_CUIDAD, default='cuidad')

   

class Formulario_Copete (models.Model):
    imagen = models.ImageField(blank=False)
    nombre = models.CharField(max_length=30)
    def __str__(self):
        return self.nombre
    precio = models.CharField(max_length=15)
    tipo = models.CharField(max_length=15)
    
    ESTADO_CHOICE = (
        ('disponible', 'Disponible'),
        ('seleccionado', 'Seleccionado'),
        ('nodisponible', 'No Disponible'),
    )
    estado = models.CharField(max_length=25, choices=ESTADO_CHOICE)
    fecha = models.DateField()



@receiver(post_delete, sender=Formulario_Copete)
def submission_delete(sender, instance, **kwargs):
    instance.imagen.delete(False)

def get_absolute_url(self):
    return reverse(
        "questions:detail",
        kwargs={
            "pk": self.pk
            }
        )

class Perfil(models.Model):
        usuario = models.OneToOneField(User, on_delete=models.CASCADE)
        bio = models.CharField(max_length=255, blank=True)
        web = models.URLField(blank=True)

        # Python 3
        def __str__(self):
                return self.usuario.username


@receiver(post_save, sender=User)
def crear_usuario_perfil(sender, instance, created, **kwargs):
        if created:
                Perfil.objects.create(usuario=instance)


@receiver(post_save, sender=User)
def guardar_usuario_perfil(sender, instance, **kwargs):
        instance.perfil.save()







