from rest_framework import serializers
from blog.models import Formulario_Copete


class Formulario_Copete_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Formulario_Copete
        fields = ('imagen', 'nombre', 'tipo','descripcion','estado', 'fecha')
