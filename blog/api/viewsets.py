from blog.models import Formulario_Copete
from .serializers import Formulario_Copete_Serializer
from rest_framework import viewsets


class Formulario_Copete_ViewSet(viewsets.ModelViewSet):
    queryset = Formulario_Copete.objects.all()
    serializer_class = Formulario_Copete_Serializer