from blog.api.viewsets import Formulario_Copete_ViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'blog', Formulario_Copete_ViewSet)