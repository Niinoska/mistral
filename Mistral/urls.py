"""Mistral URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from blog.views import SignUpView, BienvenidaView, SignInView, SignOutView, SignUpView
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from .router import router


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('blog.urls')),
    url('', include('pwa.urls')),
    url(r'^registrate/$', SignUpView.as_view(), name='sign_up'),
    url(r'^iniciar-sesion/$', SignInView.as_view(), name='sign_in'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^cerrar-sesion/$', SignOutView.as_view(), name='sign_out'),

    
    url(r'^password_reset_form/$', auth_views.PasswordResetView.as_view
     (template_name='blog/password_reset_form.html',
    html_email_template_name='blog/password_reset_email.html',), name='password_reset_form',),
    

    url(r'^recuperacion_password_reset_done/$', auth_views.PasswordResetDoneView.as_view(template_name='blog/password_reset_done.html',), name='password_reset_done',   ),
    url(r'^recuperacion_(?P<uidb64>[0-9A-Za-z_\-]+)/'
        r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',auth_views.PasswordResetConfirmView.as_view(
            success_url=('index'),
            post_reset_login=True,
            template_name='blog/password_reset_confirm.html',
            post_reset_login_backend=( 'django.contrib.auth.backends.AllowAllUsersModelBackend'
            ), ), name='password_reset_confirm', ),

    url(r'^recuperacion_password_reset_complete/$', auth_views.PasswordResetCompleteView ,
        {'template_name': 'blog/password_reset_complete.html',}, name='password_reset_complete', ),
    url(r'^recuperacion_password_reset_done/$', auth_views.PasswordResetDoneView ,
        { 'template_name': 'blog/password_reset_done.html', }, name='password_reset_done', ),
    
    url('api/', include(router.urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)





